//
//  main.swift
//  homeWork 1
//
//  Created by Pavel Belov on 28.09.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

import Foundation

//Наследование грузовика от класса движущегося средства
var scaniaTruck = Vehicle (wheels: 10, color: "black", maxSpeed: 130, year: 2020, isCargo: false, passengerSeats: 0, emergencyExits: 0 )
print("Truck has \(scaniaTruck.emergencyExits) exits")

//Соответствие пассажирского автобуса протоколу пассажирского автобуса
var volvoBus = passengerBus()
volvoBus.passengerSeats = 80
print ("Volvo BUS has \(volvoBus.passengerSeats) seats")


//Задание с замыканиями
//Внутренняя функция
func inner(_ a: Int, _ callback: @escaping (Int) -> Void) {
    print(a)
    sleep(1)
    callback(3)
}

//Внешняя функция
func outer(_ completion:  @escaping (String) -> Void) {
    print("1")
    sleep(1)
    inner (2) {
        data in
        print(data)
        sleep(1)
        completion("complete")
    }
}


outer {
    val in
    print(val)
}
