//
//  classes.swift
//  homeWork 1
//
//  Created by Pavel Belov on 28.09.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

//класс Движущееся средство
class Vehicle: passengerBusProtocol, cargoCarProtocol {
    
    //Количество колес
    var wheels: Int = 4
    //Цвет
    var color: String = "White"
    //Максимальная скорость
    var maxSpeed: Int = 120
    //Год выпуска
    var year: Int = 2020
    //Является грузовой
    var isCargo: Bool = false
    //Пассажирских сидений
    var passengerSeats: Int = 0
    //Количество выходов
    var emergencyExits: Int = 1
    
    init (wheels: Int,  color: String,  maxSpeed: Int, year: Int, isCargo: Bool, passengerSeats: Int, emergencyExits: Int) {
        
        self.wheels = wheels
        self.color = color
        self.maxSpeed = maxSpeed
        self.year = year
        self.isCargo = isCargo
        self.passengerSeats = passengerSeats
        self.emergencyExits = emergencyExits
        
    }
    
    //Движение вперед
    func moveForward () {
        print ("Moving forward")
    }
    //Движение назад
    func moveReverse () {
        print ("Moving reverse")
    }
    //Функция подъема крана вверх
    func craneUp () {
        print("Crane up")
    }
    //Функция задней разгрузки
    func rearOffLoad () {
        print ("Rear offload")
    }
}


//Структура пассажирского автобуса соответствующая протоколу
struct passengerBus: passengerBusProtocol {
    //Количество колес
    var wheels: Int = 4
    //Цвет
    var color: String = "White"
    //Максимальная скорость
    var maxSpeed: Int = 120
    //Год выпуска
    var year: Int = 2020
    //Является грузовой
    var isCargo: Bool = false
    //Пассажирских сидений
    var passengerSeats: Int = 100
    //Количество выходов
    var emergencyExits: Int = 4
    
    //Движение вперед
    func moveForward () {
        print ("Moving forward")
    }
    //Движение назад
    func moveReverse () {
        print ("Moving reverse")
    }
}
