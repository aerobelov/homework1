//
//  protocols.swift
//  homeWork 1
//
//  Created by Pavel Belov on 28.09.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

//Протокол для грузовой машины
protocol cargoCarProtocol {
    var wheels: Int {get set}
    var color: String {get set}
    var isCargo: Bool {get set}
    var year: Int {get set}
    func moveForward()
    func moveReverse()
    func craneUp()
    func rearOffLoad()
}

//Протокол для пассажирского автобуса
protocol passengerBusProtocol {
    var wheels: Int {get set}
    var color: String {get set}
    var isCargo: Bool {get set}
    var year: Int {get set}
    var emergencyExits: Int {get set}
    func moveForward()
    func moveReverse()
}
